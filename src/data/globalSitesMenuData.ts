const data = [
  {
    countries: [
      {
        label: "America Latina(espaniol)",
        link: "https://www.salesforce.com/mx/"
      },
      {
        label: "brazil",
        link: "https://www.salesforce.com/mx/"
      }
    ],
    region: "North America"
  },
  {
    countries: [
      {
        label: "America Latina(espaniol)",
        link: "https://www.salesforce.com/es/"
      },
      {
        label: "French",
        link: "https://www.salesforce.com/de"
      }
    ],
    region: "Europe"
  },
  {
    countries: [
      {
        label: "Australia",
        link: "https://www.salesforce.com/au/"
      },
      {
        label: "India",
        link: "https://www.salesforce.com/in/"
      }
    ],
    region: "Asia"
  }
];

export default data;
