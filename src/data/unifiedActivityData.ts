const data = {
  contactMenu: {
    email: ["inbox@salesforce.com"],
    heading: "Contact Us",
    links: [
      {
        label: "Request a call",
        url: "https://www.salesforce.com/form/contact/contactme.jsp"
      }
    ],
    phone: ["1-855-399-1805"]
  }
};
export default data;
