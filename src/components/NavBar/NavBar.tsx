import { library } from "@fortawesome/fontawesome-svg-core";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import * as templates from "../Template";
import "./NavBar.css";
import { INavBarObject, INavBarProps } from "./NavBarProps";
library.add(faChevronRight);

class Navbar extends React.Component<INavBarProps, object> {
  constructor(props: INavBarProps) {
    super(props);
    this.displayMenuOnEnter = this.displayMenuOnEnter.bind(this);
    this.displayContentOnClick = this.displayContentOnClick.bind(this);
    this.displayContentOnEnter = this.displayContentOnEnter.bind(this);
    this.hideContentMenu = this.hideContentMenu.bind(this);
    this.hideSubMenu = this.hideSubMenu.bind(this);
    this.handleAriaForMenu = this.handleAriaForMenu.bind(this);
    this.handleEsc = this.handleEsc.bind(this);
    this.clearOther = this.clearOther.bind(this);
  }

  public render() {
    return (
      <nav role="application">
        <ul onFocus={this.clearOther}>
          {this.props.data.map(
            (navMenuOptions: INavBarObject, index: number) => {
              return (
                <li
                  className="menu"
                  key={index}
                  tabIndex={0}
                  onKeyPress={this.displayMenuOnEnter}
                  onKeyDown={this.handleEsc}
                  onFocus={this.hideSubMenu}
                  onMouseEnter={this.handleAriaForMenu}
                  onMouseLeave={this.handleAriaForMenu}
                  aria-expanded={false}
                >
                  <a
                    href={navMenuOptions.title.url}
                    role="button"
                    tabIndex={-1}
                    id={`title_${navMenuOptions.title.title}`}
                  >
                    {navMenuOptions.title.title}
                  </a>
                  {navMenuOptions.items && (
                    <ul
                      className="megamenu"
                      aria-hidden={true}
                      aria-expanded={false}
                      aria-labelledby={`title_${navMenuOptions.title.title}`}
                    >
                      {navMenuOptions.additional && (
                        <li className="additional">
                          <ul>
                            {navMenuOptions.additional.map(names => {
                              return (
                                <li key={names.title}>
                                  <a href={names.url}> {names.title} </a>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      )}
                      <li>
                        <ul>
                          {navMenuOptions.items.map((item, indexer) => {
                            return (
                              <li
                                key={indexer}
                                className="sub-menu"
                                onClick={this.displayContentOnClick}
                                onKeyPress={this.displayContentOnEnter}
                                onFocus={this.hideContentMenu}
                              >
                                <a
                                  href={item.url}
                                  tabIndex={0}
                                  label-for="template_content"
                                >
                                  {item.title}
                                  {navMenuOptions.content &&
                                    navMenuOptions.content.find(
                                      content => content.key === item.title
                                    ) && (
                                      <span className="menu-expand">
                                        <FontAwesomeIcon icon="chevron-right" />
                                      </span>
                                    )}
                                </a>
                                {navMenuOptions.content &&
                                  navMenuOptions.content.find(
                                    content => content.key === item.title
                                  ) && (
                                    <ul
                                      className="template"
                                      role="region"
                                      id="template_content"
                                    >
                                      <li>{<templates.Sales />}</li>
                                    </ul>
                                  )}
                              </li>
                            );
                          })}
                        </ul>
                      </li>
                    </ul>
                  )}
                </li>
              );
            }
          )}
        </ul>
      </nav>
    );
  }
  // Logic to bring in keyboard accessablity for respective keyboard actions
  // TYPESCRIPT does is not friendly coupling methods for two difierent types of events.
  // So we go with individual methods for hanlding different events.

  private displayMenuOnEnter(e: React.KeyboardEvent<HTMLElement>): void {
    e.preventDefault();
    if (e.type === "keypress" && e.charCode === 13) {
      if (
        document.activeElement.className === "menu" &&
        document.activeElement.childNodes.length > 1
      ) {
        const selected = document.activeElement;
        if (selected.querySelector("#display-flex")) {
          selected.querySelector("ul")!.removeAttribute("id");
          // change ARIA-Elements accordingly.
          e.currentTarget.setAttribute("aria-expanded", "false");
          selected!.querySelector("ul")!.setAttribute("aria-hidden", "true");
          selected!.querySelector("ul")!.setAttribute("aria-expanded", "false");
          return;
        }
        // Make megamenu visible.
        selected.querySelector("ul")!.setAttribute("id", "display-flex");
        // Update Correspoding ARIA attributes for the action.
        const megamenu = selected.querySelector("ul .megamenu");
        megamenu!.setAttribute("aria-hidden", "false");
        megamenu!.setAttribute("aria-expanded", "true");
        e.currentTarget.setAttribute("aria-expanded", "true");
      } else if (
        document.activeElement.className === "menu" &&
        !document.activeElement!.querySelector("ul")
      ) {
        const linkAddress = document
          .activeElement!.querySelector("a")!
          .getAttribute("href")!;
        window.location.replace(linkAddress);
      }
    }
  }
  private handleEsc(e: React.KeyboardEvent<HTMLElement>) {
    if (e.keyCode === 27) {
      e.currentTarget.querySelector("ul .megamenu")!.removeAttribute("id");
      e.currentTarget
        .querySelector("ul .megamenu")!
        .setAttribute("aria-expanded", "false");
      e.currentTarget!.setAttribute("aria-expanded", "false");
      e.currentTarget.focus();
    }
  }
  private displayContentOnEnter(e: React.KeyboardEvent<HTMLElement>) {
    e.preventDefault();
    if (e.type === "keypress" && e.charCode === 13) {
      const subMenu = document.activeElement.parentElement;
      if (subMenu!.querySelector("#template-content")) {
        subMenu!.querySelector("ul")!.removeAttribute("id");
        return;
      }
      subMenu!.querySelector("ul")!.setAttribute("id", "template-content");
    }
  }

  private displayContentOnClick(e: React.MouseEvent<HTMLElement>) {
    e.preventDefault();
    const subMenu = document.activeElement.parentElement;
    if (subMenu!.querySelector("ul")) {
      subMenu!.querySelector("ul")!.setAttribute("id", "template-content");
    }
  }
  private hideContentMenu(e: React.SyntheticEvent<any>) {
    e.preventDefault();
    if (document.querySelector("#template-content") !== null) {
      const parentElement = document.activeElement.parentElement;
      if (parentElement!.className === "sub-menu") {
        document.querySelector("#template-content")!.removeAttribute("id");
      }
    }
  }
  private hideSubMenu(e: React.SyntheticEvent<any>) {
    e.preventDefault();
    const targetClassName = document.activeElement.className;
    if (targetClassName === "menu") {
      const elements = document.querySelectorAll(".megamenu")!;
      Array.from(elements).forEach((element: HTMLElement) => {
        element.removeAttribute("id");
        // Update aria elements accordingly.
        const megamenus = document.querySelectorAll("ul .megamenu");
        Array.from(megamenus).forEach(megamenu => {
          megamenu!.setAttribute("aria-hidden", "true");
          megamenu!.setAttribute("aria-expanded", "false");
        });
      });
    }
  }
  private clearOther(e: React.SyntheticEvent<any>) {
    e.preventDefault();
    const targetClass = document.querySelector(".usermenu__content");
    if (targetClass!.getAttribute("id") === "usermenu__content_display") {
      targetClass!.removeAttribute("id");
    }
  }
  private handleAriaForMenu(e: React.SyntheticEvent<any>) {
    if (e.type === "mouseenter") {
      if (e.currentTarget.querySelector("ul .megamenu")) {
        const menu = e.currentTarget;
        const megamenu = menu.querySelector("ul .megamenu");
        megamenu!.setAttribute("aria-hidden", "false");
        megamenu!.setAttribute("aria-expanded", "true");
        menu!.setAttribute("aria-expanded", "true");
      }
    } else if (e.type === "mouseleave") {
      const megamenus = document.querySelectorAll("ul .megamenu");
      Array.from(megamenus).forEach(megamenu => {
        megamenu!.setAttribute("aria-hidden", "true");
        megamenu!.setAttribute("aria-expanded", "false");
        megamenu!.parentElement!.setAttribute("aria-expanded", "false");
      });
    }
  }
}
export default Navbar;
