import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faAngleDown,
  faUserAstronaut
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import "./UserLoginMenu.css";
import { IUserloginMenuProps } from "./UserLoginMenuProps";

library.add(faAngleDown, faUserAstronaut);
export default class UserLoginMenu extends React.Component<
  IUserloginMenuProps
> {
  public header: any;
  constructor(props: IUserloginMenuProps) {
    super(props);
    this.showSubMenu = this.showSubMenu.bind(this);
    this.hideSubMenu = this.hideSubMenu.bind(this);
    this.displayMenuOnEnter = this.displayMenuOnEnter.bind(this);
    this.showSubMenuOnEnter = this.showSubMenuOnEnter.bind(this);
    this.hideOtherSubMenus = this.hideOtherSubMenus.bind(this);
    this.handleEsc = this.handleEsc.bind(this);
    this.header = React.createRef();
  }

  public render() {
    return (
      <ul>
        <li className="usermenu__wrapper">
          <div className="usermenu__header" aria-expanded={false}>
            <a
              tabIndex={0}
              className="usermenu__label"
              onKeyPress={this.displayMenuOnEnter}
              onFocus={this.hideOtherSubMenus}
              onKeyDown={this.handleEsc}
              ref={this.header}
            >
              {this.props.data.title}
            </a>
            <span className="usermenu__logo">
              <FontAwesomeIcon icon="user-astronaut" />
            </span>
          </div>
          <ul className="usermenu__content popup"   onKeyDown={this.handleEsc}>
            {this.props.data.container1 && (
              <li className="usermenu__container1">
                <img
                  src={this.props.data.container1.imgUrl}
                  alt={this.props.data.container1.title}
                />
                <div className="usermenu__container1_label" tabIndex={0}>
                 <span className="usermenu_a"> {this.props.data.container1.title}</span>
                </div>
              </li>
            )}

            {this.props.data.container2 && (
              <li
                className="usermenu__container2"
                onMouseEnter={this.showSubMenu}
                onMouseLeave={this.hideSubMenu}
              >
                <div className="usermenu_container2_main">
                  <div className="usermenu__container2_head">
                    <div
                      className="usermenu__container2_title"
                      tabIndex={0}
                      aria-haspopup={true}
                      onKeyPress={this.showSubMenuOnEnter}
                    >
                     <span className="usermenu_a"> {this.props.data.container2.title}</span>
                    </div>
                    <div className="usermenu__container2_sub">
                      {this.props.data.container2.subTitle}
                    </div>
                  </div>
                  <div className="usermenu__container2_font">
                    <FontAwesomeIcon icon="angle-down" />
                  </div>
                </div>
                <ul className="usermenu_container2_slidein" aria-hidden={true}>
                  <li>
                    {this.props.data.container2 &&
                      this.props.data.container2.urls.map(url => {
                        return (
                          <div
                            className="usermenu_container2_url"
                            key={url.label}
                          >
                            <a href={`https://${url.url}`}> {url.label}</a>
                          </div>
                        );
                      })}
                  </li>
                </ul>
              </li>
            )}
          </ul>
        </li>
      </ul>
    );
  }
  private showSubMenu(e: React.MouseEvent<HTMLElement>) {
    e.preventDefault();
    e.currentTarget
      .querySelector(".usermenu_container2_slidein")!
      .setAttribute("id", "displayAdditional");
  }
  private hideSubMenu(e: React.MouseEvent<HTMLElement>) {
    e.preventDefault();
    e.currentTarget
      .querySelector(".usermenu_container2_slidein")!
      .removeAttribute("id");
  }
  private displayMenuOnEnter(e: React.KeyboardEvent<HTMLElement>): void {
    e.preventDefault();
    if (e.type === "keypress" && e.charCode === 13) {
      const content = document.querySelector(".usermenu__content")!;
      content.getAttribute("id") === "usermenu__content_display"
        ? (() => {
            content.removeAttribute("id");

            document
              .querySelector(".usermenu__label")!
              .setAttribute("aria-expanded", "false");
          })()
        : (() => {
            content.setAttribute("id", "usermenu__content_display");
            document
              .querySelector(".usermenu__label")!
              .setAttribute("aria-expanded", "true");
          })();
    }
  }
  private hideOtherSubMenus(e: React.FocusEvent<HTMLElement>) {
    e.preventDefault();
    const popups = Array.from(document.querySelectorAll(".popup")!);
    for (const element of popups) {
      element.removeAttribute("id");
    }
  }
  private handleEsc(e: React.KeyboardEvent<HTMLElement>) {
    if (e.keyCode === 27) {
      const content = document.querySelector(".usermenu__content")!;
      content.removeAttribute("id");
      this.header.current.focus();
      document
        .querySelector(".usermenu__label")!
        .setAttribute("aria-expanded", "false");
    }
  }
  private showSubMenuOnEnter(e: React.KeyboardEvent<HTMLElement>): void {
    const content = e.currentTarget.parentElement!.parentElement!.parentElement!.querySelector(
      ".usermenu_container2_slidein"
    )!;
    if (content.getAttribute("id") === "displayAdditional") {
      content.removeAttribute("id");
      content.setAttribute("aria-hidden", "true");
    } else {
      content.setAttribute("id", "displayAdditional");
      content.setAttribute("aria-hidden", "false");
    }
  }
}
