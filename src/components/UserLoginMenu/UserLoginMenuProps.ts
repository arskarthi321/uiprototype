export interface IUserloginMenuProps {
  data: IContactMenuData;
}
interface IContactMenuData {
  title: string;
  container1: IContainer1;
  container2: IContainer2;
}
interface IContainer1 {
  imgUrl: string;
  title: string;
}
interface IContainer2 {
  title: string;
  subTitle: string;
  urls: IUrl[];
}
interface IUrl {
  label: string;
  url: string;
}
