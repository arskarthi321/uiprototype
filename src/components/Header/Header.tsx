import * as React from "react";
import SalesForceLogo from "../../assets/salesforce-logo-offl.png";
import navBarData from "../../data/navbarData";
import Navbar from "../NavBar/NavBar";
import UnifiedActivity from "../UnifiedActivity/UnifiedActivity";
import "./Header.css";
const Header = (props: any) => {
  return (
    <header id="header-section" role="application">
      <div id="title-section" aria-label="salesforce logo">
        <img src={SalesForceLogo} alt="Salesforce Logo" className="logo" />
      </div>
      <div id="activity-section">
        <UnifiedActivity />
        <div id="nav-bar" role="application">
          <Navbar data={navBarData} />
        </div>
      </div>
    </header>
  );
};
export default Header;
