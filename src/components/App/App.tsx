import * as React from "react";
import Body from "../Body/Body";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";

import "./App.css";

class App extends React.Component {
  constructor(props: {}) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  public render() {
    return (
      <div id="app-wrapper" onClick={this.handleClick}>
        <header>
          <Header />
        </header>
        <div id="container">
          <Body />
        </div>
        <footer>
          <Footer />
        </footer>
      </div>
    );
  }

  private handleClick(e: React.MouseEvent<HTMLElement>) {
    e.stopPropagation();
    if (document.activeElement.closest("nav")) {
      return;
    } else if (document.activeElement.closest("#UnifiedActivity")) {
      return;
    }
    Array.from(e.currentTarget.querySelectorAll(".megamenu")).map(item => {
      item.removeAttribute("id");
    });
    Array.from(e.currentTarget.querySelectorAll(".menu, .megamenu")!).map(
      item => {
        item.setAttribute("aria-expanded", "false");
      }
    );
    Array.from(e.currentTarget.querySelectorAll(".popup")).map(popup => {
      popup.removeAttribute("id");
    });
  }
}

export default App;
