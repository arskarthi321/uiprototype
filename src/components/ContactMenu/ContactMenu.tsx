import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faEnvelope,
  faHeadset,
  faPhone
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import "./ContactMenu.css";
import { IContactMenuProps } from "./ContactMenuProps";

library.add(faHeadset, faPhone, faEnvelope);
export default class ContactMenu extends React.Component<IContactMenuProps> {
  public header: any;
  constructor(props: IContactMenuProps) {
    super(props);
    this.displayMenuOnEnter = this.displayMenuOnEnter.bind(this);
    this.handleEsc = this.handleEsc.bind(this);
    this.handleMouseIn = this.handleMouseIn.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.hideOtherSubMenus = this.hideOtherSubMenus.bind(this);
    this.header = React.createRef();
  }

  public render() {
    return (
      <ul
        className="contactmenu__wrapper"
        onMouseEnter={this.handleMouseIn}
        onMouseLeave={this.handleMouseOut}
      >
        <li>
          <div
            className="contactmenu__header"
            id="contactmenu__header"
            tabIndex={0}
            onKeyPress={this.displayMenuOnEnter}
            onFocus={this.hideOtherSubMenus}
            onKeyDown={this.handleEsc}  
            ref={this.header}
            aria-haspopup={true}
            aria-expanded={false}
          >
            {this.props.data.heading}
          </div>
          <ul
            className="contactmenu__content popup"
            onKeyDown={this.handleEsc}
            aria-labelledby={`contactmenu__header`}
          >
            {this.props.data.phone &&
              this.props.data.phone.map((listItem, index) => {
                return (
                  <li key={index}>
                    <div>
                      <span className="font">
                        <FontAwesomeIcon icon="phone" />
                      </span>
                      <a href={`tel://${listItem}`}> {listItem}</a>
                    </div>
                  </li>
                );
              })}
            {this.props.data.email &&
              this.props.data.email.map((listItem, index) => {
                return (
                  <li key={index}>
                    <div>
                      <span className="font">
                        <FontAwesomeIcon icon="envelope" />
                      </span>
                      {
                        <a href={`mailto:${listItem}`} target="_top">
                          {listItem}
                        </a>
                      }
                    </div>
                  </li>
                );
              })}
            {this.props.data.links &&
              this.props.data.links.map((listItem, index) => {
                return (
                  <li key={index}>
                    <div>
                      <span className="font">
                        <FontAwesomeIcon icon="headset" />
                      </span>
                      <a href={listItem.url}>{listItem.label}</a>
                    </div>
                  </li>
                );
              })}
          </ul>
        </li>
      </ul>
    );
  }
  private displayMenuOnEnter(e: React.KeyboardEvent<HTMLElement>): void {
    e.preventDefault();
    if (e.type === "keypress" && e.charCode === 13) {
      if (document.activeElement.className === "contactmenu__header") {
        const content = document.querySelector(".contactmenu__content")!;
        content.getAttribute("id") === "contactmenu__display"
          ? (() => {
              content.removeAttribute("id");
              document
                .querySelector(".contactmenu__header")!
                .setAttribute("aria-expanded", "false");
            })()
          : (() => {
              content.setAttribute("id", "contactmenu__display");
              document
                .querySelector(".contactmenu__header")!
                .setAttribute("aria-expanded", "true");
            })();
      }
    }
  }
  private handleEsc(e: React.KeyboardEvent<HTMLElement>) {
    if (e.keyCode === 27) {
      const content = document.querySelector(".contactmenu__content")!;
      content.removeAttribute("id");
      this.header.current.focus();
      document
        .querySelector(".contactmenu__header")!
        .setAttribute("aria-expanded", "false");
    }
  }

  private handleMouseIn(e: React.MouseEvent<HTMLElement>) {
    document
      .querySelector(".contactmenu__header")!
      .setAttribute("aria-expanded", "true");
  }

  private handleMouseOut(e: React.MouseEvent<HTMLElement>) {
    document
      .querySelector(".contactmenu__header")!
      .setAttribute("aria-expanded", "false");
  }
  private hideOtherSubMenus(e: React.FocusEvent<HTMLElement>) {
    e.preventDefault();
    const popups = Array.from(document.querySelectorAll(".popup")!);
    for (const element of popups) {
      element.removeAttribute("id");
    }
  }
}
