import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import "./GlobalSiteMenu.css";
import { IGlobalSiteMenuProps } from "./GlobalSiteMenuProps";
export default class GlobalSiteMenu extends React.Component<
  IGlobalSiteMenuProps
> {
  public header: any;
  constructor(props: IGlobalSiteMenuProps) {
    super(props);
    this.displayMenuOnEnter = this.displayMenuOnEnter.bind(this);
    this.handleEsc = this.handleEsc.bind(this);
    this.hideOtherSubMenus = this.hideOtherSubMenus.bind(this);
    this.handleMouseIn = this.handleMouseIn.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.header = React.createRef();
  }
  public render() {
    return (
      <ul
        className="gs_menu_wrapper"
        onMouseEnter={this.handleMouseIn}
        onMouseLeave={this.handleMouseOut}
      >
        <li>
          <div
            className="gs_menu__label, gs_menu_ua-icon "
            aria-label="region selector"
            onKeyPress={this.displayMenuOnEnter}
            onFocus={this.hideOtherSubMenus}
            onKeyDown={this.handleEsc}
            tabIndex={0}
            aria-expanded={false}
            aria-haspopup={true}
            id="gs_menu_heading"
            ref={this.header}
          >
            <FontAwesomeIcon icon="globe" />
          </div>
          <ul
            className="gs_menu__content popup"
            aria-labelledby="gs_menu_heading"
            onKeyDown={this.handleEsc}
          >
            <li className="gs_menu_globalist">
              {this.props.data.map((region, index) => {
                return (
                  <div key={index}>
                    <ul>
                      <a href="#" className="gs_menu_region">
                        {region.region}
                      </a>
                      <ul>
                        {region.countries.map((country, serialno) => {
                          return (
                            <li key={serialno} className="gs_menu_country">
                              <a href={country.link}>{country.label}</a>
                            </li>
                          );
                        })}
                      </ul>
                    </ul>
                  </div>
                );
              })}
            </li>
          </ul>
        </li>
      </ul>
    );
  }
  private displayMenuOnEnter(e: React.KeyboardEvent<HTMLElement>): void {
    e.preventDefault();
    if (e.type === "keypress" && e.charCode === 13) {
      const content = document.querySelector(".gs_menu__content")!;
      content.getAttribute("id") === "gs_content_display"
        ? (() => {
            content.removeAttribute("id");

            document
              .querySelector("#gs_menu_heading")!
              .setAttribute("aria-expanded", "false");
          })()
        : (() => {
            content.setAttribute("id", "gs_content_display");
            document
              .querySelector("#gs_menu_heading")!
              .setAttribute("aria-expanded", "true");
          })();
    }
  }
  private handleEsc(e: React.KeyboardEvent<HTMLElement>) {
    if (e.keyCode === 27) {
      const content = document.querySelector(".gs_menu__content")!;
      content.removeAttribute("id");
      this.header.current.focus();
      document
        .querySelector("#gs_menu_heading")!
        .setAttribute("aria-expanded", "false");
    }
  }
  private hideOtherSubMenus(e: React.FocusEvent<HTMLElement>) {
    e.preventDefault();
    const popups = Array.from(document.querySelectorAll(".popup")!);
    for (const element of popups) {
      element.removeAttribute("id");
    }
  }
  private handleMouseIn(e: React.MouseEvent<HTMLElement>) {
    document
      .querySelector("#gs_menu_heading")!
      .setAttribute("aria-expanded", "true");
  }
  private handleMouseOut(e: React.MouseEvent<HTMLElement>) {
    document
      .querySelector("#gs_menu_heading")!
      .setAttribute("aria-expanded", "false");
  }
}
