export interface IGlobalSiteMenuProps {
  data: IGLobalMenuData[];
}
interface IGLobalMenuData {
  countries: ICountriesInterface[];
  region: string;
}
interface ICountriesInterface {
  label: string;
  link: string;
}
