<h1>UI Prototype</h1>

<h2>Setup</h2>

<h3>Main Dependencies</h3>

1.  [Node](http://nodejs.org) (`node`)
1.  [Yarn](https://yarnpkg.com/lang/en/) (`yarn`)

Download and install main dependencies for your specific environment.

<h2>Running</h2>

<h3>Local Devlopment Server</h3>

1.  Open a terminal in project root.
2.  Run `yarn` - Required only when installing packages.
3.  Run `yarn start` this will bootup the local devlopment server.
4.  On success, you can view the website in any browser at `http://localhost:3000/` - It will open automatically in your default browser

<h2>Landmark Files to author navigation bar</h2>

1.  Data Configuration - /src/data/navbarData.ts
2.  Data type reference for navigation menu - /src/components/NavBar/NavBarProps.ts

<h2>Landmark Files to author other menus</h2>

1.  Data Configuration - /src/data/globalSiteMenuData.ts, userLoginMenuData.ts,
    unifiedActivityData.ts (for contact menu).
2.  Data type reference for menus - /src/components/NavBar/globalSiteMenuProps.ts, globalSiteMenuProps, ContactMenuProps.
